#!/bin/bash
echo Welcome to simple kube deployment
if test "$#" -lt 1; then
    kubectl create namespace hello
    kubectl apply -k mysql
    kubectl apply -k app
    kubectl get services -A
else
    kubectl --kubeconfig $1 create namespace hello
    kubectl --kubeconfig $1 apply -k mysql
    kubectl --kubeconfig $1 apply -k app
    kubectl --kubeconfig $1 get services -A
fi
