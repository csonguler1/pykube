@echo off
title simple kubernetes deployment
echo Welcome to simple kube deployment

if "%1" == "" goto noconfig
start /b /wait kubectl --kubeconfig %1 create namespace hello
start /b /wait kubectl --kubeconfig %1 apply -k mysql
start /b /wait kubectl --kubeconfig %1 apply -k app
start /b /wait kubectl --kubeconfig %1 get services -A
exit /b 1

:noconfig
start /b /wait kubectl create namespace hello
start /b /wait kubectl apply -k mysql
start /b /wait kubectl apply -k app
start /b /wait kubectl get services -A
