#!/usr/bin/env bash
# first argument is join script second argument is ip address
sudo $1
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip='$2'"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
sudo systemctl daemon-reload
sudo systemctl restart kubelet