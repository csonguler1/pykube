#!/usr/bin/env bash
# first argument join script, second argument ip address 
OUTPUT_FILE=$1
IP=$2
KUBEADM_DEBUG_FILE=/vagrant/provision/temp/kubeadm-output.txt
rm -rf $OUTPUT_FILE
rm -rf $KUBEADM_DEBUG_FILE

# Start cluster
sudo kubeadm init --apiserver-advertise-address=$IP --pod-network-cidr=10.244.0.0/16 > ${KUBEADM_DEBUG_FILE}
cat ${KUBEADM_DEBUG_FILE} | sed -e ':begin;/\\$/{N;bbegin};s/\\\n//g' | grep "kubeadm join" > ${OUTPUT_FILE}
chmod +x $OUTPUT_FILE

# Configure kubectl
mkdir -p $HOME/.kube
sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
# Copy config for external access
sudo cp /etc/kubernetes/admin.conf /vagrant/provision/temp

# Fix kubelet IP
echo 'Environment="KUBELET_EXTRA_ARGS=--node-ip='$IP'"' | sudo tee -a /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

# Configure flannel
kubectl apply -f /vagrant/provision/kube-flannel.yaml

sudo systemctl daemon-reload
sudo systemctl restart kubelet