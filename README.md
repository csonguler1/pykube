# pykube

## Creating a Kubernetes Cluster
We use Vagrant in order to simulate the production environment in the best way.  
So you have to first install 
- [Vagrant](https://www.vagrantup.com/docs/installation#installing-vagrant) 
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads) (since we use it as provider)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

Just follow the instructions for your Host OS, it shouldn't be a problem.
Afterwards you just have to run
```
vagrant up
```
and the cluster should be set up. 
In order to get a kubectl on some other computer (e.g. laptop) to talk to your cluster, you need to copy the administrator kubeconfig file from your control-plane node to your workstation.  
Vagrant script copies this kubeconfig file (admin.conf) under ./provision/temp/  
Now you should be able to use it to check the status of the nodes:  
```
kubectl --kubeconfig ./provision/temp/admin.conf get nodes
```
You can append the content to your configuration or copy the file to somewhere else and use with --kubeconfig option.  
Or you can specify the kubeconfig file to use by setting the KUBECONFIG environment. Probably this option might be easiest.  
#### Notes
- Please read the [Running Multiple Hypervisors](https://www.vagrantup.com/docs/installation#running-multiple-hypervisors) section in Vagrant documentation.  
Virtualbox does not run together with other hypervisors.  
- Using a single master node and removing the taint to run pods would be easier but then we could also use minikube or similar in that case. To better represent production environment, default is 1 master + 2 worker nodes  
- Installing docker via vagrant provisioning is possible but somehow it takes too much time. I decided to install it with provision script.   
- For the persistency I had planned to use /vagrant/data folder as hostPath. Since all the nodes share this folder I thought it was a simple and good solution. However you get a permission error (95 and 195) on Windows, I am not sure about Linux. So I use a hostPath which is not shared. Downside is if the pod is scheduled to another node then the data will be lost. I added this as an issue to the project.  
## Building and Running the Application  
Application is a Python application, so you'll need python and pip installed on your host.  
mySQLdb is a python interface for mysql, but it is not mysql itself. And apparently mySQLdb needs the command 'mysql_config'.  
So, you probably need to install mysql-config under Linux
```
sudo apt-get install libmysqlclient-dev
```
I didn't face this problem under Windows. Still be aware, my environment wasn't a fully clean one.  
After that you can build as a typical Python application. Recommendation is to use virtualenv.
```
cd app
python -m venv nyenv
pip install -r requirements.txt
``` 
Source code is under src folder. 
You can use [Gunicorn](https://gunicorn.org/) as WSGI HTTP server. I think it is best to install it to system. 
See https://docs.gunicorn.org/en/stable/install.html  
For Ubuntu:
```
$ sudo apt-get update
$ sudo apt-get install gunicorn
```
Gunicorn expects a module name, not a path, so you have to use --chdir parameter or run gunicorn under src
```
gunicorn --bind 0.0.0.0:3000 --chdir src server:application
```
gunicorn also supports reloading. So you can use 
```
gunicorn --reload --bind 0.0.0.0:3000 --chdir src server:application
```
during local development.  
You can also just call for standard Flask dev server:  
```
python src/server.py
```
As container registry I used gitlab's container registry. Since the project is public, container registry is also public.  
Still, you'll need to login: 
```
docker login registry.gitlab.com
```
Then you can build & push. Provided Dockerfile has multi stage build and uses gunicorn. 
```
docker build -t registry.gitlab.com/csonguler1/pykube/hello .
docker push registry.gitlab.com/csonguler1/pykube/hello
```
I am assuming that you have access to project. If not, you can use another registry and change the image path in kube/app/hello-deployment.yaml
## Deploying the application
Our application uses MySQL as database, this means we will also need a MySQL deployment.  
A kustomization file is created for MySQL and also one for the application deployment.  
You can run
```
kubectl apply -k kube/mysql
kubectl apply -k kube/app
``` 
A namespace can be specified in kustomization.yaml files. I used a namespace called "hello" in kustomization files to be able to quickly destroy resources if needed.  
Be careful when you use the yaml files directly, they don't have namespaces defined in them.    
I also created a very basic script for Linux and Windows which just run kubectl commands.  
You need to specify the kubeconfig location as the first argument to these scripts.  
After running a deploy script everything should be ready.  
Check the kube folder if you want to inspect the configuration.  
A NodePort service is created to access our application. Its port number is 31001. 
You should be able to access application via http://<NodeIP>:<NodePort>  
I was able to access it with http://10.0.0.10:31001  
## Development in Kubernetes
A Kubernetes deployment uses images from a container registry.  
So, in order to see a code change you need to build and push your application to a registry and make sure that newer image is used in the Kubernetes pod.  
This is a lot of work.  
There are some approaches like [Telepresence](https://www.telepresence.io/) which finds a solution to this problem with a proxy.  
However I liked [Skaffold](https://skaffold.dev/)'s approach most. It is pretty straight-forward. It listens to the changes in your environment, 
builds docker image automatically, pushes to the specified registry and handles the manifests for Kubernetes.  
It is not blazing fast but it can be used in any environment where Docker runs. It also has some other features which I want to check later.  
First install it from https://skaffold.dev/docs/install/  
Then when you run skaffold dev with a repo specified you will see that it starts to watch your changes and deploy them.  
You must be logged in to the registry.  
Example:  
```
docker login registry.gitlab.com
skaffold dev --default-repo=registry.gitlab.com/csonguler1/pykube
```
And that's it. Just be aware that File sync can only update files that can be modified by the container’s configured User ID.  
So, it is best that you update files in the same environment and user where you run skaffold command.  
Check skaffold.yaml and hello-pod.yaml files under app folder for settings.  
Development pod for our application is named "hello-dev".  
Since our application is a web application and we want to be able to access it externally I also created a NodePort service for testing.  
Its name is "hello-dev-service". Namespace is the same. Another approach could be to separate namespaces.  
You should be able to access dev application via http://<NodeIP>:<NodePort>  
I was able to access it with http://10.0.0.10:31002  

You can also scale down the prod app with 
```
kubectl scale --replicas=0 deployment/hello-deployment -n hello
```
when you want to just use "hello-dev" application.  


Another option is to expose everything via NodePort services, modify the cfg and run the application locally which might not be always possible.